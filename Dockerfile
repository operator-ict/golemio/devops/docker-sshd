FROM ubuntu:24.04

RUN apt-get update && \
    apt-get install -y openssh-server vim; \
    mkdir /var/run/sshd; \
    echo 'root:root' |chpasswd

RUN sed -ri 's/^PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config; \
    sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config

EXPOSE 22

CMD    ["/usr/sbin/sshd", "-D", "-e"]
